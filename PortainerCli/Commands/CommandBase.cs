﻿using CommandLine;

namespace PortainerCli.Commands
{
    public class CommandBase
    {
        [Option("apiurl", Required = true, HelpText = "The portainer url to use for this command.")]
        public string ApiUrl { get; set; }
    }
}
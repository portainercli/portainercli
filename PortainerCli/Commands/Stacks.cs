﻿using CommandLine;
using MediatR;
using PortainerCli.Attributes;
using PortainerCli.Services.PortainerClient;
using PortainerCli.Services.PortainerClient.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using CommandLine.Text;

namespace PortainerCli.Commands
{
    [Verb("stacks", HelpText = "Manage stacks.")]
    [ChildVerbs(typeof(Deploy), typeof(Remove))]
    public class Stacks
    {
        [Verb("deploy", HelpText = "Deploy a stack.")]
        public class Deploy : AuthenticatedCommand, IRequest<ReturnCode>
        {
            [Value(0, Required = true, HelpText = "The path to the compose file.")]
            public string ComposeFile { get; set; }

            [Option("endpoint", Required = true, HelpText = "The name of the endpoint to deploy the stack on.")]
            public string EndpointName { get; set; }

            [Option("name", Required = true, HelpText = "The name of the stack to deploy.")]
            public string Name { get; set; }

            [Option("swarmid", Required = true, HelpText = "The id of the swarm to deploy the stack on.")]
            public string SwarmId { get; set; }

            [Option("env",
                HelpText = "A JSON formatted array of name/value pairs representing environment variables for the stack.")]
            public string Env { get; set; }

            [Option("update", HelpText = "Update the stack if it already exists.")]
            public bool Update { get; set; }

            [Option("prune", HelpText = "Prune services that are no longer referenced.")]
            public bool Prune { get; set; }

            [Usage]
            public static IEnumerable<Example> Examples =>
                new List<Example>
                {
                    new Example("Create a new stack if one doesn't exist already", new Deploy
                    {
                        ApiUrl = "https://portainer.example.com/api",
                        Username = "username",
                        Password = "password",
                        ComposeFile = "docker-compose.yml",
                        EndpointName = "local",
                        Name = "mystack",
                        SwarmId = Guid.Empty.ToString()
                    }),
                    new Example("Create a new stack or update an existing stack", new Deploy
                    {
                        ApiUrl = "https://portainer.example.com/api",
                        Username = "username",
                        Password = "password",
                        ComposeFile = "docker-compose.yml",
                        EndpointName = "local",
                        Name = "mystack",
                        SwarmId = Guid.Empty.ToString(),
                        Update = true,
                        Prune = true
                    }),
                    new Example("Provide environment variables when deploying a stack", new Deploy
                    {
                        ApiUrl = "https://portainer.example.com/api",
                        Username = "username",
                        Password = "password",
                        ComposeFile = "docker-compose.yml",
                        EndpointName = "local",
                        Name = "mystack",
                        SwarmId = Guid.Empty.ToString(),
                        Env = "[{\"name\": \"MY_VARIABLE\", \"value\": \"somevalue\"}," +
                              "{\"name\": \"MY_OTHER_VARIABLE\", \"value\": \"someothervalue\"}]"
                    })
                };

            public class Handler : IRequestHandler<Deploy, ReturnCode>
            {
                public async Task<ReturnCode> Handle(Deploy request, CancellationToken cancellationToken)
                {
                    var portainerClient = new PortainerClient(request.ApiUrl, request.Username, request.Password);

                    var env = JsonSerializer.Deserialize<List<StackEnv>>(request.Env, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    var stackFileContent = await ReadComposeFile(request.ComposeFile);

                    var endpoint = (await portainerClient.Endpoints.List())
                        .SingleOrDefault(e =>
                            string.Equals(e.Name, request.EndpointName, StringComparison.InvariantCultureIgnoreCase));
                    if (endpoint == null)
                    {
                        Console.WriteLine("No endpoint found with that name.");

                        return ReturnCode.Error;
                    }

                    var filter = new
                    {
                        SwarmId = request.SwarmId,
                        EndpointId = endpoint.Id
                    };
                    var stack = (await portainerClient.Stacks.List(JsonSerializer.Serialize(filter)))
                        .SingleOrDefault(e =>
                            string.Equals(e.Name, request.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (stack != null && !request.Update)
                    {
                        Console.WriteLine("Stack already exists.");

                        return ReturnCode.Error;
                    }

                    if (stack == null)
                    {
                        var stackCreateRequest = new StackCreateRequest
                        {
                            Name = request.Name,
                            SwarmId = request.SwarmId,
                            StackFileContent = stackFileContent,
                            Env = env
                        };

                        await portainerClient.Stacks.Create(
                            (int)StackType.Swarm,
                            StackCreateMethod.String,
                            endpoint.Id,
                            stackCreateRequest);

                        return ReturnCode.Success;
                    }

                    var stackUpdateRequest = new StackUpdateRequest
                    {
                        StackFileContent = stackFileContent,
                        Prune = request.Prune,
                        Env = env
                    };

                    await portainerClient.Stacks.Update(stack.Id, endpoint.Id, stackUpdateRequest);

                    return ReturnCode.Success;
                }

                private async Task<string> ReadComposeFile(string composeFilePath)
                {
                    using var stackFile = File.OpenText(composeFilePath);

                    return await stackFile.ReadToEndAsync();
                }
            }
        }

        [Verb("remove", HelpText = "Remove a stack.")]
        public class Remove : AuthenticatedCommand, IRequest<int>
        {
            public class Handler : IRequestHandler<Remove, int>
            {
                public Task<int> Handle(Remove request, CancellationToken cancellationToken)
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}
﻿using CommandLine;
using MediatR;
using PortainerCli.Attributes;
using PortainerCli.Services.PortainerClient;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using PortainerCli.Services.PortainerClient.Requests;

namespace PortainerCli.Commands
{
    [Verb("endpoints", HelpText = "Manage endpoints.")]
    [ChildVerbs(typeof(List))]
    public class Endpoints
    {
        [Verb("createjob", HelpText = "Create a job on the specified endpoint.")]
        public class CreateJob : AuthenticatedCommand, IRequest<ReturnCode>
        {
            [Option("endpoint", Required = true, HelpText = "The name of the endpoint to deploy the stack on.")]
            public string EndpointName { get; set; }

            [Option("image", Required = true, HelpText = "The docker image to use for the job.")]
            public string Image { get; set; }

            [Option("script", Required = true, HelpText = "The script to run.")]
            public string Script { get; set; }

            public class Handler : IRequestHandler<CreateJob, ReturnCode>
            {
                public async Task<ReturnCode> Handle(CreateJob request, CancellationToken cancellationToken)
                {
                    var portainerClient = new PortainerClient(request.ApiUrl, request.Username, request.Password);

                    var endpoint = (await portainerClient.Endpoints.List())
                        .SingleOrDefault(e =>
                            string.Equals(e.Name, request.EndpointName, StringComparison.InvariantCultureIgnoreCase));
                    if (endpoint == null)
                    {
                        Console.WriteLine("No endpoint found with that name.");

                        return ReturnCode.Error;
                    }
                    
                    var jobCreateRequest = new EndpointJobCreateRequest
                    {
                        Image = request.Image,
                        FileContent = request.Script
                    };

                    await portainerClient.Endpoints.CreateJob(endpoint.Id, jobCreateRequest);

                    return ReturnCode.Success;
                }
            }
        }
    
        [Verb("list", HelpText = "List endpoints.")]
        public class List : AuthenticatedCommand, IRequest<ReturnCode>
        {
            public class Handler : IRequestHandler<List, ReturnCode>
            {
                public async Task<ReturnCode> Handle(List request, CancellationToken cancellationToken)
                {
                    var portainerClient = new PortainerClient(request.ApiUrl, request.Username, request.Password);

                    var endpoints = await portainerClient.Endpoints.List();

                    foreach (var endpoint in endpoints)
                    {
                        Console.WriteLine(JsonSerializer.Serialize(endpoint));
                    }

                    return ReturnCode.Success;
                }
            }
        }
    }
}
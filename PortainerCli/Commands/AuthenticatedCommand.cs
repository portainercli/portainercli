﻿using CommandLine;

namespace PortainerCli.Commands
{
    public abstract class AuthenticatedCommand : CommandBase
    {
        [Option("username", Required = true, HelpText = "The portainer user to log in as.")]
        public string Username { get; set; }

        [Option("password", Required = true, HelpText = "The password for the portainer user.")]
        public string Password { get; set; }
    }
}
﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace PortainerCli
{
    public static class Program
    {
        static async Task<int> Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);

            var application = new Application(services);

            return await application.Run(args);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }

        public static int Error()
        {
            return (int) ReturnCode.Error;
        }

        public static int Error(string message)
        {
            Console.WriteLine(message);

            return (int) ReturnCode.Error;
        }

        public static int Success()
        {
            return (int) ReturnCode.Success;
        }
    }
}

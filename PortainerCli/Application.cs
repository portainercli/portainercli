﻿using CommandLine;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using PortainerCli.Commands;
using PortainerCli.Extensions;
using System.Threading.Tasks;

namespace PortainerCli
{
    public class Application
    {
        private readonly IMediator _mediator;

        public Application(IServiceCollection serviceCollection)
        {
            var services = serviceCollection.BuildServiceProvider();

            _mediator = services.GetRequiredService<IMediator>();
        }

        public async Task<int> Run(string[] args)
        {
            return await Parser.Default.ParseVerbs<Endpoints, Stacks>(args)
                .MapResult((IRequest<ReturnCode> e) => HandleCommand(e),
                    error => Task.FromResult(Program.Error()));
        }

        private async Task<int> HandleCommand<TRequest>(TRequest request)
            where TRequest : IRequest<ReturnCode>
        {
            return (int) await _mediator.Send(request);
        }
    }
}
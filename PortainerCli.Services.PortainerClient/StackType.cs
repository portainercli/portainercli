﻿namespace PortainerCli.Services.PortainerClient
{
    public enum StackType
    {
        Swarm = 1,
        Compose = 2
    }
}
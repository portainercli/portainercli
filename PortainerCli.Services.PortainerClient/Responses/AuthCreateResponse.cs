﻿namespace PortainerCli.Services.PortainerClient.Responses
{
    public class AuthCreateResponse
    {
        public string Jwt { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace PortainerCli.Services.PortainerClient.Responses
{
    public class StackResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public StackType Type { get; set; }
        public int EndpointId { get; set; }
        public string EntryPoint { get; set; }
        public string SwarmId { get; set; }
        public string ProjectPath { get; set; }
        public List<StackEnv> Env { get; set; }
    }
}
﻿namespace PortainerCli.Services.PortainerClient.Responses
{
    public class EndpointResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EndpointType Type { get; set; }
        public string Url { get; set; }
        public string PublicUrl { get; set; }
        public int GroupId { get; set; }
    }
}
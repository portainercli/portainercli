﻿using PortainerCli.Services.PortainerClient.Responses;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;
using PortainerCli.Services.PortainerClient.Requests;

namespace PortainerCli.Services.PortainerClient.Endpoints
{
    [Headers("Authorization: Bearer")]
    public interface IEndpointsEndpoint
    {
        [Get("/endpoints")]
        Task<List<EndpointResponse>> List();

        [Post("/endpoints/{endpointId}/job?method=string")]
        Task CreateJob(int endpointId, [Body] EndpointJobCreateRequest request);
    }
}
﻿using PortainerCli.Services.PortainerClient.Requests;
using PortainerCli.Services.PortainerClient.Responses;
using Refit;
using System.Threading.Tasks;

namespace PortainerCli.Services.PortainerClient.Endpoints
{
    public interface IAuthEndpoint
    {
        [Post("/auth")]
        Task<AuthCreateResponse> Create([Body] AuthCreateRequest request);
    }
}
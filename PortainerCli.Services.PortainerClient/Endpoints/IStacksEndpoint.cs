﻿using PortainerCli.Services.PortainerClient.Requests;
using PortainerCli.Services.PortainerClient.Responses;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PortainerCli.Services.PortainerClient.Endpoints
{
    [Headers("Authorization: Bearer")]
    public interface IStacksEndpoint
    {
        [Get("/stacks")]
        Task<List<StackResponse>> List(string filters);

        [Post("/stacks")]
        Task<StackResponse> Create(int type, string method, int endpointId, [Body] StackCreateRequest request);

        [Put("/stacks/{stackId}")]
        Task<StackResponse> Update(int stackId, int endpointId, [Body] StackUpdateRequest request);
    }
}
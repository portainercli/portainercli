﻿namespace PortainerCli.Services.PortainerClient
{
    public enum EndpointType
    {
        Docker = 1,
        Agent = 2,
        Azure = 3
    }
}
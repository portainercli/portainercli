﻿using PortainerCli.Services.PortainerClient.Authentication;
using PortainerCli.Services.PortainerClient.Endpoints;
using PortainerCli.Services.PortainerClient.Requests;
using Refit;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;

namespace PortainerCli.Services.PortainerClient
{
    public class PortainerClient : IPortainerClient
    {
        private readonly string _username;
        private readonly string _password;
        private readonly IAuthEndpoint _authEndpoint;

        private string _jwt;

        public IEndpointsEndpoint Endpoints { get; }
        public IStacksEndpoint Stacks { get; }

        public PortainerClient(string baseAddress, string username, string password)
        {
            _username = username;
            _password = password;

            var httpClientHandler = new AuthenticatedHttpClientHandler(GetJwt);
            var httpClient = new HttpClient(httpClientHandler)
            {
                BaseAddress = new Uri(baseAddress.TrimEnd('/'))
            };

            _authEndpoint = RestService.For<IAuthEndpoint>(httpClient);
            Endpoints = RestService.For<IEndpointsEndpoint>(httpClient);
            Stacks = RestService.For<IStacksEndpoint>(httpClient);
        }

        private async Task<string> GetJwt()
        {
            if (!ValidateToken(_jwt))
            {
                var request = new AuthCreateRequest
                {
                    Username = _username,
                    Password = _password
                };
                var response = await _authEndpoint.Create(request);

                _jwt = response.Jwt;
            }

            return _jwt;
        }

        private static bool ValidateToken(string jwt)
        {
            if (string.IsNullOrWhiteSpace(jwt)) return false;

            var token = new JwtSecurityToken(jwt);
            if (DateTime.UtcNow < token.ValidFrom) return false;
            if (DateTime.UtcNow > token.ValidTo) return false;

            return true;
        }
    }
}
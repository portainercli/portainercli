﻿using PortainerCli.Services.PortainerClient.Endpoints;

namespace PortainerCli.Services.PortainerClient
{
    public interface IPortainerClient
    {
        IEndpointsEndpoint Endpoints { get; }
        IStacksEndpoint Stacks { get; }
    }
}
﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace PortainerCli.Services.PortainerClient.Authentication
{
    public class AuthenticatedHttpClientHandler : HttpClientHandler
    {
        private readonly Func<Task<string>> _getJwt;

        public AuthenticatedHttpClientHandler(Func<Task<string>> getJwt)
        {
            _getJwt = getJwt;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var auth = request.Headers.Authorization;
            if (auth?.Scheme == "Bearer")
            {
                var jwt = await _getJwt();
                request.Headers.Authorization = new AuthenticationHeaderValue(auth.Scheme, jwt);
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
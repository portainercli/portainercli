﻿using System.Collections.Generic;

namespace PortainerCli.Services.PortainerClient.Requests
{
    public class StackUpdateRequest
    {
        public string StackFileContent { get; set; }
        public List<StackEnv> Env { get; set; } = new List<StackEnv>();
        public bool Prune { get; set; }
    }
}
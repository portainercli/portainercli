﻿namespace PortainerCli.Services.PortainerClient.Requests
{
    public static class StackCreateMethod
    {
        public const string File = "file";
        public const string String = "string";
        public const string Repository = "repository";
    }
}
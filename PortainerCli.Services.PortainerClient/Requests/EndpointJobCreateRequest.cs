﻿namespace PortainerCli.Services.PortainerClient.Requests
{
    public class EndpointJobCreateRequest
    {
        public string Image { get; set; }
        public string FileContent { get; set; }
    }
}
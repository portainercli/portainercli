﻿namespace PortainerCli.Services.PortainerClient.Requests
{
    public class AuthCreateRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace PortainerCli.Services.PortainerClient.Requests
{
    public class StackCreateRequest
    {
        public string Name { get; set; }
        public string SwarmId { get; set; }
        public string StackFileContent { get; set; }
        public string RepositoryUrl { get; set; }
        public string RepositoryReferenceName { get; set; }
        public string ComposeFilePathInRepository { get; set; }
        public bool RepositoryAuthentication { get; set; }
        public string RepositoryUsername { get; set; }
        public string RepositoryPassword { get; set; }
        public List<StackEnv> Env { get; set; }
    }
}
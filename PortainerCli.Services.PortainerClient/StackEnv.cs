﻿namespace PortainerCli.Services.PortainerClient
{
    public class StackEnv
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}